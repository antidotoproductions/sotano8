using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// MMC
/// Esta funci�n tan s�lo establece la selecci�n en un bot�n del men� principal cuando se viene del men� de carga. Si no se
/// hace, se pierde el foco y no se puede navegar con teclado o gamepad.
/// </summary>

public class MainMenuBtn : MonoBehaviour
{
    #region Variables

    public Button btnMainMenuDefault;


    #endregion Variables

    #region Funciones de Unity
    public void SelectButton()
    {
        btnMainMenuDefault.Select();
    }

    #endregion Funciones de Unity
}
