using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;


public class PreMenu : MonoBehaviour
{

    public GameObject goPreMenuTimeline;
    public GameObject goPressAnyKey;
    public GameObject goMenus;
    public GameObject goDefaultBtnSelection;
    public GameObject goMenuMusic;
    public GameObject goMyself;
    



    void Update()
    {
        AnyKey();
    }

    private void AnyKey()
    {
        if (Keyboard.current.anyKey.IsPressed())//|| Mouse.current.leftButton.isPressed||Gamepad.current.IsPressed())
        {
            StartCoroutine(Open2Menus());
        }

        if (Mouse.current.leftButton.isPressed)
        {
            StartCoroutine(Open2Menus());
        }

    }


    IEnumerator Open2Menus()
    {
        goPressAnyKey.SetActive(false);
        goPreMenuTimeline.SetActive(true);
        goMenus.SetActive(true);
        goDefaultBtnSelection.SetActive(true);
        yield return new WaitForSeconds(2f);
        goMenuMusic.SetActive(true);
        goMyself.SetActive(false);

    }
}
