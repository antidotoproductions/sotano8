using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using UnityEngine.UI;

public class ElevatorManager : MonoBehaviour
{
    public Flowchart flowchart;
    public Text elevatorText;
    

    private void Update()
    {
        int invFloor = -flowchart.GetIntegerVariable("floor");
        elevatorText.text = flowchart.GetIntegerVariable("floor").ToString();
        flowchart.SetIntegerVariable("invFloor", invFloor);
        
    }

    
}
